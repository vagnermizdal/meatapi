import { Server } from './server/server';
import { usersRouter } from './users/users.router';
import { restaurantsRouter } from './restaurants/restaurants.router';
import { reviewsRouter } from './reviews/reviews.router';

const serve = new Server();

serve.bootstrap([
    usersRouter,
    restaurantsRouter,
    reviewsRouter
]).then(serve => {
    console.log("Server is listing on: ", serve.application.address());
}).catch((error => {
    console.log("Start fail");
    console.error(error);
    process.exit(1);
}))


