import 'jest'
import * as request from 'supertest'

let address = (<any>global).address

test('get /users', () => {
    return request(address)
        .get('/users')
        .then(response => {
            expect(response.status).toBe(200)
            expect(response.body.items).toBeInstanceOf(Array)
        })
        .catch(fail)
})

test('post /users', () => {
    return request(address)
        .post('/users')
        .send({
            name: 'Usuario 1',
            email: 'usuario1@email.com',
            password: '123456',
            cpf: '63573093035'
        })
        .then(response => {
            expect(response.status).toBe(200)
            expect(response.body._id).toBeDefined()
            expect(response.body.name).toBe('Usuario 1')
            expect(response.body.email).toBe('usuario1@email.com')
            expect(response.body.cpf).toBe('63573093035')
            expect(response.body.password).toBeUndefined()
        })
        .catch(fail)
})

test('get /users/aaaaa - not found', () => {
    return request(address)
        .get('/users/aaaaa')
        .then(response => {
            expect(response.status).toBe(404)
        })
        .catch(fail)
})

test('patch /users/:id', () => {
    return request(address)
        .post('/users')
        .send({
            name: 'Usuario 2',
            email: 'usuario2@email.com',
            password: '123456'
        })
        .then(response => {
            return request(address)
                .patch(`/users/${response.body._id}`)
                .send({
                    name: 'Usuario 2 patch'
                })
                .then(response => {
                    expect(response.status).toBe(200)
                    expect(response.body._id).toBeDefined()
                    expect(response.body.name).toBe('Usuario 2 patch')
                    expect(response.body.email).toBe('usuario2@email.com')
                })
                .catch(fail)
        })
        .catch(fail)
})